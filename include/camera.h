#ifndef CAMERA_H
#define CAMERA_H

#include "utils.h"

/**
 * Camera, as a moving point with direction
 */
typedef struct Camera
{
    vec3 position;
    vec3 rotation;
    vec3 speed;
    float abs_speed;
} Camera;

/**
 * Is the texture preview visible?
 */
int is_preview_visible;

/**
 * Initialize the camera to the start position.
 */
void init_camera(Camera *camera);

/**
 * Update the position of the camera.
 */
void update_camera(Camera *camera, double time);

/**
 * Apply the camera settings to the view transformation.
 */
void set_view(const Camera *camera);

/**
 * Set the horizontal and vertical rotation of the view angle.
 */
void rotate_camera(Camera *camera, double horizontal, double vertical);

/**
 * Set the speed of forward and backward motion.
 */
void set_camera_speed(Camera *camera, double speed);

/**
 * Set the speed of left and right side steps.
 */
void set_camera_side_speed(Camera *camera, double speed);

/**
 * Set the speed of up and down motion.
 */
void set_camera_vertical_speed(Camera *camera, double speed);

/**
 * Move camera forward according to its rotation. 
 */
void move_camera_forward(Camera *camera);

/**
 * Move camera backward according to its rotation. 
 */
void move_camera_backward(Camera *camera);

/**
 * Move camera into forward or backward according to its rotation and direction. 
 */
void move_camera(Camera *camera, int direction);

/**
 * Move camera left according to its rotation. 
 */
void move_camera_left(Camera *camera);

/**
 * Move camera right according to its rotation. 
 */
void move_camera_right(Camera *camera);

/**
 * Move camera side according to its rotation and direction. 
 */
void move_camera_side(Camera *camera, int direction);

/**
 * Move camera up.
 */
void move_camera_up(Camera *camera);

/**
 * Move camera down.
 */
void move_camera_down(Camera *camera);

/**
 * Move camera up or down according to given direction.
 */
void move_camera_vertical(Camera *camera, int direction);

/**
 * Move camera backward according its rotation. 
 */
void stop_camera(Camera *camera);

#endif /* CAMERA_H */
