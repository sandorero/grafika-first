#ifndef OSD_H
#define OSD_H

#include "camera.h"

/**
 * Draw an on screen display.
 */
void draw_osd(Camera *camera);

#endif /* OSD_H */
