#include "callbacks.h"
#include "osd.h"
#include "utils.h"
#include <math.h>
#include <GL/glut.h>
#include <stdio.h>

#define VIEWPORT_RATIO (4.0 / 3.0)
#define VIEWPORT_ASPECT 50.0
#define CAMERA_SPEED 1.0

struct
{
    int x;
    int y;
} mouse_position;

int osd_visible = 0;

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glPushMatrix();
    set_view(&camera);
    draw_scene(&scene);
    glPopMatrix();

    if (osd_visible)
    {
        draw_osd(&camera);
    }
    //if (is_preview_visible) {
    //    show_texture_preview();
    //}

    glutSwapBuffers();
}

void reshape(GLsizei width, GLsizei height)
{
    int x, y, w, h;
    double ratio;

    ratio = (double)width / height;
    if (ratio > VIEWPORT_RATIO)
    {
        w = (int)((double)height * VIEWPORT_RATIO);
        h = height;
        x = (width - w) / 2;
        y = 0;
    }
    else
    {
        w = width;
        h = (int)((double)width / VIEWPORT_RATIO);
        x = 0;
        y = (height - h) / 2;
    }

    glViewport(x, y, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(VIEWPORT_ASPECT, VIEWPORT_RATIO, 0.01, 10000.0);
}

void mouse(int button, int state, int x, int y)
{
    mouse_position.x = x / 2;
    mouse_position.y = y / 2;
}

void motion(int x, int y)
{
    x = x / 2; //float xx = x * 2.0; // / glutGet(GLUT_WINDOW_HEIGHT) / 2);
    y = y / 2; //float yy = (y / glutGet(GLUT_WINDOW_WIDTH) / 2);

    rotate_camera(&camera, mouse_position.x - x, mouse_position.y - y);
    mouse_position.x = x;
    mouse_position.y = y;

    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 'e':
        move_camera_up(&camera);
        break;
    case 'q':
        move_camera_down(&camera);
        break;
    case 'w':
        move_camera_forward(&camera);
        break;
    case 's':
        move_camera_backward(&camera);
        break;
    case 'a':
        move_camera_left(&camera);
        break;
    case 'd':
        move_camera_right(&camera);
        break;
    case 't':
        if (is_preview_visible)
        {
            is_preview_visible = FALSE;
        }
        else
        {
            is_preview_visible = TRUE;
        }
        break;
    case GLUT_KEY_F1:
    case 'h':
        if (osd_visible)
        {
            osd_visible = 0;
        }
        else
        {
            osd_visible = 1;
        }
        break;
    case 27:
        glutExit();
    }

    glutPostRedisplay();
}

void keyboard_up(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 'e':
    case 'q':
    case 'w':
    case 's':
    case 'a':
    case 'd':
        stop_camera(&camera);
        break;
    case 'r':
        init_camera(&camera);
    }

    glutPostRedisplay();
}

void idle()
{
    static int last_frame_time = 0;
    int current_time;
    double elapsed_time;

    current_time = glutGet(GLUT_ELAPSED_TIME);
    elapsed_time = (double)(current_time - last_frame_time) / 1000;
    last_frame_time = current_time;

    update_camera(&camera, elapsed_time);

    glutPostRedisplay();
}
