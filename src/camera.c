#include <math.h>
#include <GL/glut.h>

#include "camera.h"
#include "utils.h"

void init_camera(Camera *camera)
{
    camera->position.x = -3.0;
    camera->position.y = -1.5;
    camera->position.z = 2.0;
    camera->rotation.x = 330.0;
    camera->rotation.y = 0.0;
    camera->rotation.z = 30.0;
    camera->speed.x = 0.0;
    camera->speed.y = 0.0;
    camera->speed.z = 0.0;
    camera->abs_speed = 1;

    is_preview_visible = FALSE;
}

void update_camera(Camera *camera, double time)
{
    double angle;
    double side_angle;
    double vertical_angle;

    angle = degree_to_radian(camera->rotation.z);
    side_angle = degree_to_radian(camera->rotation.z);
    vertical_angle = degree_to_radian(camera->rotation.x);

    camera->position.x += camera->speed.x * time;
    camera->position.y += camera->speed.y * time;
    camera->position.z += camera->speed.z * time;
    camera->position.x += camera->speed.x * time;
    camera->position.y += camera->speed.y * time;
    camera->position.z += camera->speed.z * time;
}

void set_view(const Camera *camera)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glRotatef(-(camera->rotation.x + 90), 1.0, 0, 0);
    glRotatef(-(camera->rotation.z - 90), 0, 0, 1.0);
    glTranslatef(-camera->position.x, -camera->position.y, -camera->position.z);
}

void rotate_camera(Camera *camera, double horizontal, double vertical)
{
    camera->rotation.z += horizontal;
    camera->rotation.x += vertical;

    if (camera->rotation.z < 0)
    {
        camera->rotation.z += 360.0;
    }

    if (camera->rotation.z > 360.0)
    {
        camera->rotation.z -= 360.0;
    }

    if (camera->rotation.x < 0)
    {
        camera->rotation.x += 360.0;
    }

    if (camera->rotation.x > 360.0)
    {
        camera->rotation.x -= 360.0;
    }
}

void set_camera_speed(Camera *camera, double speed)
{
    camera->speed.y = speed;
}

void set_camera_side_speed(Camera *camera, double speed)
{
    camera->speed.x = speed;
}

void set_camera_vertical_speed(Camera *camera, double speed)
{
    camera->speed.z = speed;
}

void move_camera_forward(Camera *camera)
{
    move_camera(camera, 1);
}

void move_camera_backward(Camera *camera)
{
    move_camera(camera, -1);
}

void move_camera(Camera *camera, int direction)
{
    double zspeed = cos(degree_to_radian(camera->rotation.x));
    camera->speed.x = direction * camera->abs_speed * zspeed * cos(degree_to_radian(camera->rotation.z));
    camera->speed.y = direction * camera->abs_speed * zspeed * sin(degree_to_radian(camera->rotation.z));
    camera->speed.z = direction * camera->abs_speed * sin(degree_to_radian(camera->rotation.x));
}

void move_camera_left(Camera *camera)
{
    move_camera_side(camera, 1);
}

void move_camera_right(Camera *camera)
{
    move_camera_side(camera, -1);
}

void move_camera_side(Camera *camera, int direction)
{
    double zspeed = cos(degree_to_radian(camera->rotation.x));
    camera->speed.x = direction * camera->abs_speed * zspeed * cos(degree_to_radian(camera->rotation.z + 90));
    camera->speed.y = direction * camera->abs_speed * zspeed * sin(degree_to_radian(camera->rotation.z + 90));
    camera->speed.z = direction * camera->abs_speed * sin(degree_to_radian(camera->rotation.x));
}

void move_camera_up(Camera *camera)
{
    move_camera_vertical(camera, 1);
}

void move_camera_down(Camera *camera)
{
    move_camera_vertical(camera, -1);
}

void move_camera_vertical(Camera *camera, int direction)
{
    double zspeed = cos(degree_to_radian(camera->rotation.x + 90));
    camera->speed.x = direction * camera->abs_speed * zspeed * cos(degree_to_radian(camera->rotation.z + 180));
    camera->speed.y = direction * camera->abs_speed * zspeed * sin(degree_to_radian(camera->rotation.z + 180));
    camera->speed.z = direction * camera->abs_speed * sin(degree_to_radian(camera->rotation.x + 90));
}

void stop_camera(Camera *camera)
{
    camera->speed.x = 0.0;
    camera->speed.y = 0.0;
    camera->speed.z = 0.0;
}

void show_texture_preview()
{
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glColor3f(1, 1, 1);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(-1, 1, -3);
    glTexCoord2f(1, 0);
    glVertex3f(1, 1, -3);
    glTexCoord2f(1, 1);
    glVertex3f(1, -1, -3);
    glTexCoord2f(0, 1);
    glVertex3f(-1, -1, -3);
    glEnd();

    glDisable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
}
