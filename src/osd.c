#include "osd.h"
#include <math.h>
#include <stdio.h>

#include <GL/glut.h>

double vmax = 0;

void draw_osd(Camera *camera)
{
    double vabs = sqrt(pow(camera->speed.x, 2) + pow(camera->speed.y, 2) + pow(camera->speed.z, 2));
    if (vabs > vmax)
    {
        vmax = vabs;
    }
    glBegin(GL_2D);
    glColor3f(1, 1, 1);
    glWindowPos2i(0, 100);
    char osd_template[] = "Camera\n------\nx: %2.2f deg: %2.0f v: %2.2f\ny: %2.2f deg: %2.0f v: %2.2f\nz: %2.2f deg: %2.0f v: %2.2f\nv abs: %2.2f, v abs max: %2.2f\n";
    char osd[sizeof(osd_template) + 11 * 12];
    sprintf(osd, osd_template, camera->position.x, camera->rotation.x, camera->speed.x, camera->position.y, camera->rotation.y, camera->speed.y, camera->position.z, camera->rotation.z, camera->speed.z, vabs, vmax);
    glutBitmapString(GLUT_BITMAP_8_BY_13, osd);
    glEnd();
}
