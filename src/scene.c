#include "scene.h"

#include <GL/glut.h>

#include "../lib/obj/include/load.h"
#include "../lib/obj/include/draw.h"

void init_scene(Scene *scene)
{
    load_model(&(scene->model), "models/gun.obj");
    scene->texture_id = load_texture("textures/metal.png");

    glBindTexture(GL_TEXTURE_3D, scene->texture_id);

    scene->material.ambient.red = 0.19225;
    scene->material.ambient.green = 0.19225;
    scene->material.ambient.blue = 0.19225;

    scene->material.diffuse.red = 0.50754;
    scene->material.diffuse.green = 0.50754;
    scene->material.diffuse.blue = 0.50754;

    scene->material.specular.red = 0.508273;
    scene->material.specular.green = 0.508273;
    scene->material.specular.blue = 0.508273;

    scene->material.shininess = 0.9;
}

void set_lighting()
{
    float ambient_light[] = {10.0f, 10.0f, 10.0f, 1.0f};
    float diffuse_light[] = {0.2f, 0.2f, 0.2, 1.0f};
    float specular_light[] = {0.1f, 0.1f, 0.1f, 1.0f};

    float position[] = {-5.0f, -3.0f, 10.0f, 0.0f};

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_light);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular_light);
    glLightfv(GL_LIGHT0, GL_POSITION, position);
}

void set_material(const Material *material)
{
    float ambient_material_color[] = {
        material->ambient.red,
        material->ambient.green,
        material->ambient.blue};

    float diffuse_material_color[] = {
        material->diffuse.red,
        material->diffuse.green,
        material->diffuse.blue};

    float specular_material_color[] = {
        material->specular.red,
        material->specular.green,
        material->specular.blue};

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient_material_color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse_material_color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular_material_color);

    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &(material->shininess));
}

void draw_scene(const Scene *scene)
{

    set_material(&(scene->material));
    set_lighting();
    glColor3f(0.2, 0.2, 0.2);

    draw_model(&(scene->model));

    draw_origin();
}

void draw_origin()
{

    glBegin(GL_LINES);

    glColor3f(1, 0, 0);
    glVertex3f(-10, 0, 0);
    glVertex3f(10, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, -10, 0);
    glVertex3f(0, 10, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, -10);
    glVertex3f(0, 0, 10);

    glEnd();
}